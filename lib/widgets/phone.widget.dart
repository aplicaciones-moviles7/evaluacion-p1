import 'package:flutter/cupertino.dart';
import 'package:mzaa_eval_p1/models/phone.model.dart';

class PhoneWidget extends StatelessWidget {
  final Phone phone;
  final Function onTap;

  const PhoneWidget({Key? key, required this.phone, required this.onTap});

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap(phone);
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(phone.title, style: TextStyle(fontWeight: FontWeight.w600),),
                      Text(phone.price.toStringAsFixed(0), style: TextStyle(fontWeight: FontWeight.bold),),                      
                    ],
                  ),
                  Text(phone.description),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
