import 'package:flutter/material.dart';
import 'package:mzaa_eval_p1/data/Phone.data.dart';
import 'package:mzaa_eval_p1/models/phone.model.dart';
import 'package:mzaa_eval_p1/screens/DetailPhone.ui.dart';
import 'package:mzaa_eval_p1/widgets/phone.widget.dart';

class ListPhone extends StatefulWidget {
  const ListPhone({Key? key}) : super(key: key);

  @override
  _ListPhone createState() => _ListPhone();
}

class _ListPhone extends State<ListPhone> {
  var listPhones = PhoneData().getPhone();

  void showDetailPhone(Phone phone) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DetailPhone(phone: phone);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Andrés Moncayo - 8vo Transición'),
      ),
      body: ListView.separated(
        itemCount: listPhones.length,
        itemBuilder: (BuildContext context, index) =>
            PhoneWidget(phone: listPhones[index], onTap: showDetailPhone),
        separatorBuilder: (BuildContext context, index) => const Divider(
          color: Colors.black45,
          indent: 25,
          endIndent: 25,
        ),
      ),
    );
  }
}
