import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mzaa_eval_p1/models/phone.model.dart';

class DetailPhone extends StatelessWidget {
  final Phone phone;
  const DetailPhone({Key? key, required this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(phone.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  phone.brand.toUpperCase(),
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  phone.price.toStringAsFixed(0),
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.deepOrange,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            Image.network(phone.image),
            const SizedBox(
              height: 16.0,
            ),
            const Text(
              "Descripción:",
              style:
                  TextStyle(fontWeight: FontWeight.w600, color: Colors.black87),
            ),
            Text(phone.description),
          ],
        ),
      ),
    );
  }
}
