import 'package:mzaa_eval_p1/models/phone.model.dart';

class PhoneData {
  static final PhoneData phoneData = PhoneData._internal();

  factory PhoneData() {
    return phoneData;
  }

  PhoneData._internal();

  final listPhone = [
    Phone(
        id: 1,
        title: "iPhone 9",
        description: "An apple mobile which is nothing like apple",
        price: 549,
        brand: "Apple",
        image: "https://i.dummyjson.com/data/products/1/1.jpg"),
    Phone(
        id: 2,
        title: "iPhone X",
        description:
            "SIM-Free, Model A19211 6.5-inch Super Retina HD display with OLED technology A12 Bionic chip with ...",
        price: 899,
        brand: "Apple",
        image: "https://i.dummyjson.com/data/products/2/1.jpg"),
    Phone(
        id: 3,
        title: "Samsung Universe 9",
        description:
            "Samsung's new variant which goes beyond Galaxy to the Universe",
        price: 1249,
        brand: "Samsung",
        image: "https://i.dummyjson.com/data/products/3/1.jpg"),
    Phone(
        id: 4,
        title: "OPPOF19",
        description: "OPPO F19 is officially announced on April 2021.",
        price: 280,
        brand: "OPPO",
        image: "https://i.dummyjson.com/data/products/4/1.jpg"),
    Phone(
        id: 5,
        title: "Huawei P30",
        description:
            "Huawei’s re-badged P30 Pro New Edition was officially unveiled yesterday in Germany and now the device has made its way to the UK.",
        price: 499,
        brand: "Huawei",
        image: "https://i.dummyjson.com/data/products/5/1.jpg"),
  ];

  List<Phone> getPhone() {
    return listPhone;
  }
   
}
