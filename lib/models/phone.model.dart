import 'dart:ffi';

class Phone {

  final int id;
  final String title;
  final String description;
  final double price;
  final String brand;
  final String image;

  Phone({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.brand,
    this.image = '',
  });
  
}